import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { LoginComponent } from '../auth/login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  faBars = faBars;

  showMenu = false;

  toggleNavbar(){
    this.showMenu = !this.showMenu;
  }

  constructor(public dialog: MatDialog) {}

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(LoginComponent, {
      width: '500px',
      enterAnimationDuration,
      exitAnimationDuration,
    });
  }

}
