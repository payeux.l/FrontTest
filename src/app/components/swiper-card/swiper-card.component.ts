import { Component } from '@angular/core';

import SwiperCore, { EffectCards } from "swiper";

SwiperCore.use([EffectCards]);

@Component({
  selector: 'app-swiper-card',
  templateUrl: './swiper-card.component.html',
  styleUrls: ['./swiper-card.component.scss']
})
export class SwiperCardComponent {

}
