import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  signInForm!: FormGroup;
  //user = User.GetNewInstance();
  loading = false;
  emailExists = ! null;
  passwordType = "password";

  // AUTH //
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  
  email!: string;
  password!:string;

  constructor(public dialogRef: MatDialogRef<LoginComponent>,
    private formBuilder: FormBuilder,) {}

  ngOnInit(): void {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.compose([ValidationService.emailValidator, Validators.required])],
      password: ['', Validators.required],
    });
  }

  /**
   * Perform a sign in request and configure the associated local storage.
   */
  signInRequest() {
    console.log(this.signInForm.value.email + "  " + this.signInForm.value.password)
    /*
    this.alertService.clear();
    this.loading = true;
    console.log("TEST : " + this.signInForm.value.email + " AND " + this.signInForm.value.password);
    this.user.email = this.signInForm.value.email;
    this.user.password = this.signInForm.value.password;
    console.log(this.user.email);

    this.authService.login(this.user.email!, this.user.password!).subscribe({
      next: data => {
        this.storageService.saveUser(data);
        this.loading = false;
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.storageService.getUser().roles;
        //this.reloadPage();
        if (this.casierName != null) {
          this.router.navigate(['./' + this.casierName]);
        } else {
          this.router.navigate(['./connect/profile']);
        }
      },
      error: err => {
        this.loading = false;
        this.errorMessage = err.message;
        this.isLoginFailed = true;
      }
    });
      */
    
   }

}
