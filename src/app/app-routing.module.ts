import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './components/connect/signup/signup.component';
import { FirstpageComponent } from './components/firstpage/firstpage.component';

const routes: Routes = [
  { path: '', component: FirstpageComponent },
  {path: 'signup', component: SignupComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy: PreloadAllModules,
    },
  )],

  exports: [RouterModule]
})
export class AppRoutingModule { }
