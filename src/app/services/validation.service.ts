import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  /**
   * Check whether an email is valid according to its format.
   * @param control
   * @returns {any}
   */
  static emailValidator(control: FormControl) {
    let regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (regExp.test(control.value)) {
      return (null);
    } else {
      return ({"invalidEmailAddress": true});
    }
  }
  static emailCheck(email: string): boolean {
    let regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (regExp.test(email)) return true;
    else return false;
  }

  /**
   * Check whether a login is valid according to its format.
   * @param control
   * @returns {any}
   */
  static loginValidator(control: FormControl) {
    //let regExp = /^[a-zA-Z0-9.\-_ ]{3,30}$/;
    let regExp = /^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._ \s-']{3,35}$/
    if (regExp.test(control.value)) {
      return (null);
    } else {
      return ({"invalidLogin": true});
    }
  }

  /**
   * Check whether a login is valid according to its format.
   * @param control
   * @returns {any}
   */
  static communityNameValidator(control: FormControl) {
    //let regExp = /^[a-zA-Z0-9.\-_ ]{3,30}$/;
    let regExp = /^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._ \s-()'&]{3,50}$/
    if (regExp.test(control.value)) {
      return (null);
    } else {
      return ({"invalidLogin": true});
    }
  }

  /**
   * Check whether a name is valid according to its format.
   * @param control
   * @returns {any}
   */
  static nameValidator(value: string) {
    let regExp = /^[a-zA-Z0-9-_]{3,20}$/
    if (regExp.test(value)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Check whether a string is valid according to its length.
   * @param control
   * @param length
   * @returns {any}
   */
  static communityDescriptionLengthValidator(control: FormControl) {
    if (control.value == null || control.value.length <= 3000) {
      return (null);
    } else {
      return ({"invalidLength": true});
    }
  }

  /**
   * Check whether a string is valid according to its length.
   * @param control
   * @returns {any}
   */
  static placeDescriptionLengthValidator(control: FormControl) {
    if (control.value == null || control.value.length <= 200) {
      return (null);
    } else {
      return ({"invalidLength": true});
    }
  }

  /**
   * Check whether a string is valid according to its length.
   * @param control
   * @returns {any}
   */
  static communityPasswordLengthValidator(control: FormControl) {
    if (control.value == null || control.value.length <= 50) {
      return (null);
    } else {
      return ({"invalidLength": true});
    }
  }

  /**
   * Check whether the user age is between 18 and 100.
   * @param control
   * @returns {any}
   */
  static ageValidator(control: FormControl) {
    if (control.value >= 16 && control.value <= 200) {
      return (null);
    } else {
      return ({"invalidAge": true});
    }
  }

  /**
   * Check whether a password is valid according to its format.
   * {6,100}         - Password length between 6 and 100
   * (?=.*[0-9])     - Password contains at least one number
   * @param control
   * @returns {any}
   */
  static passwordValidator(control: FormControl) {
    let regExp = /^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/;
    if (regExp.test(control.value)) {
      return (null);
    } else {
      return ({"invalidPassword": true});
    }
  }

  /**
   * Check whether a password is equal to a base password.
   *
   * @param passwordControl
   * @param passwordCheckControl
   * @returns {(group:FormGroup)=>{[p: string]: any}}
   */
  static passwordCheckValidator(passwordControl: string, passwordCheckControl: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordControl];
      let confirmPassword = group.controls[passwordCheckControl];
      if (password.value == confirmPassword.value) {
        confirmPassword.setErrors(null);
        return null!;
      } else {
        confirmPassword.setErrors({"passwordMismatch": true });
        return ({"passwordMismatch": true });
      }
    }
  }

  /**
   * Check whether a input respect a specific size.
   * @param size
   * @param control
   * @returns {any}
   */
  static sizeValidator(control: FormControl) {
    if (control.value != null && control.value.length <= 4000) {
      return (null);
    } else {
      return ({"invalidSize": true});
    }
  }
}
